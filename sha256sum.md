# Sha256sum

## Compare sha256 sum with file
```bash
echo 5eb9dc96cccbdfe7610d3cbced1bd6ee89b5acdfc83ffee1f06e6d02b058390c kali-linux-2024.2-installer-amd64.iso | sha256sum -c
```
