# Curl

1. Send JSON

```bash
curl --data '{"name":"bob"}' --header 'Content-Type: application/json' -X POST http://localhost/data
```

2. Measure time

```bash
cat <<EOF > timing.template
\n\n
     time_namelookup:  %{time_namelookup}s\n
        time_connect:  %{time_connect}s\n
     time_appconnect:  %{time_appconnect}s\n
    time_pretransfer:  %{time_pretransfer}s\n
       time_redirect:  %{time_redirect}s\n
  time_starttransfer:  %{time_starttransfer}s\n
                     ----------\n
          time_total:  %{time_total}s\>
EOF
curl -w "@curl-format.txt" "http://localhost:3000"
```
