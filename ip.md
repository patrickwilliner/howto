# Ip

```bash
# show device details for eno1
ip -d link show dev eno1

# put device into promisc mode
ip link set eno1 promisc on
```

