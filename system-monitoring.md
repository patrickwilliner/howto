# System monitoring

1. top

| Key | Usage                                                                                                                                    |
|-----|------------------------------------------------------------------------------------------------------------------------------------------|
|  t  | Displays summary information off and on.                                                                                                 |
|  m  | Displays memory information off and on.                                                                                                  |
|  A  | Sorts the display by top consumers of various system resources. Useful for quick identification of performance-hungry tasks on a system. |
|  f  | Enters an interactive configuration screen for top. Helpful for setting up top for a specific task.                                      |
|  o  | Enables you to interactively select the ordering within top.                                                                             |
|  r  | Issues renice command.                                                                                                                   |
|  k  | Issues kill command.                                                                                                                     |
|  z  | Turn on or off color/mono                                                                                                                |

---

2. vmstat

The vmstat command reports information about processes, memory, paging, block IO, traps, and cpu activity.


