# Markdown

## Heading
\# H1
\#\# H2
\#\#\# H3

## Bold
\*\*bold text\*\*

## Italic
\*italic text\*

## Blockquote
\> blockquote

## Ordered list
1\. First
2\. Second
3\. Third

## Unordered list
\- First
\- Second
\- Third

## Code
\`some code\`

## Horizontal rule
\-\-\-

## Link
\[title\](https://www.example.com)

## Image
\!\[alt text\](image.jpg)

---

## Table
\| Syntax \| Description \|
\| ----------- \| ----------- \|
\| Header \| Title \|
\| Paragraph \| Text \| 
