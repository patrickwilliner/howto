# Xargs

## Show actual command that is run with -t 

```bash
seq 5 | xargs -t
```

## xargs without arguments will call echo

## Define and use placeholder

```bash
ls | xargs -I {} echo "__{}__"
```

## run per line
```bash
ls | xargs -t -n 1
```
