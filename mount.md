# Mount

## Mount img file

```bash
fdisk -l Lakka-Generic.x86_64-3.2.img
sudo mount -o loop,offset=$((4202496*512)) Lakka-Generic.x86_64-3.2.img /media/usb2

losetup /dev/loop10 Lakka-Generic.x86_64-3.2.img
partprobe /dev/loop10
gparted /dev/loop10
losetup -d /dev/loop10

truncate --size=8589934592 Lakka-Generic.x86_64-3.2.img
```
