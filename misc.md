# Misc

chage - change user password expiry information
umask - define default file permission pattern
last, lastb - show listing of last logged in users
realpath - print the resolved path
lsb_release - print info about distribution 
cat /etc/os-release - print os release info
sudo -EH etherape - run wayland GUI application
nohup - run a command immune to hangups, with output to a non-tty
arping - send ARP request to a host
TMOUT=300 - autologout user after 300 seconds
faillock
sudoedit
lynis audit system
glow
