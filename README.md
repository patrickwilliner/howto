# Howtos and little helper scripts

List of tips and tricks around shell commands and other neat stuff.

Global access with the following script (uses fzf and mdp):

```
#!/bin/sh

# Get the directory of the script
howto_dir=$HOME/Data/Documents/Howto/
result=$(find $howto_dir -type f -name '*.md' | fzf)

# If a file is selected, pipe the result to mdp
if [[ -n $result ]]; then
    mdp <(cat $result)
fi
```
