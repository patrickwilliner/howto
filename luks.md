# Luks

## Encrypt disk

```
cryptsetup luksFormat /dev/sdb1
cryptsetup open /dev/sdb1 encrypted
mkfs.ext4 /dev/mapper/encrypted
```


## Open encrypted disk

```
cryptsetup --type luks open /dev/sdb1 encrypted
mount -t ext4 /dev/mapper/encrypted /place/to/mount
```


## Close encrypted disk

```
umount /place/to/mount
cryptsetup close encrypted
```

---

# Add key file

1. List keyslots

```
cryptsetup luksDump /dev/sdb1
```


2. Create new key file

```
dd if=/dev/random bs=32 count=1 of=/root/lukskey
```


3. Check new key file

```
xxd /root/lukskey
```


4. Add key to luks file

```
cryptsetup luksAddKey /dev/sdb1 /root/lukskey
```


5. Verify that the new keyslot is enabled

```
cryptsetup luksDump /dev/sdb1
```
