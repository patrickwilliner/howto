# Rescue via grub edit

1. Hit e at grub selection
2. Add init=/bin/bash to the end of the kernel command line
3. Remount rw
    ```
    mount -o remount,rw /
    ```
