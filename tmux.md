# Tmux

## start new with session name:

```bash
tmux new -s myname
```


## attach:

```bash
tmux a  #  (or at, or attach)
```


## attach to named:

```bash
tmux a -t myname
```


## list sessions:

```bash
tmux ls
```


## kill session:

```bash
tmux kill-session -t myname
```


## Kill all the tmux sessions:

```bash
tmux ls | grep : | cut -d. -f1 | awk '{print substr($1, 0, length($1)-1)}' | xargs kill
```

---

Hit the prefix `ctrl+a` (default prefix is ctrl+b) and then:

## List all shortcuts

?

## Sessions

:new<CR>  new session
s  list sessions
$  name session

## <a name="WindowsTabs"></a>Windows (tabs)

c  create window
w  list windows
n  next window
p  previous window
f  find window
,  name window
&  kill window

## <a name="PanesSplits"></a>Panes (splits) 

%  vertical split
"  horizontal split

o  swap panes
q  show pane numbers
x  kill pane
+  break pane into window (e.g. to select text by mouse to copy)
-  restore pane from window
⍽  space - toggle between layouts
<prefix> q (Show pane numbers, when the numbers show up type the key to goto that pane)
<prefix> { (Move the current pane left)
<prefix> } (Move the current pane right)
<prefix> z toggle pane zoom

## <a name="syncPanes"></a>Sync Panes 

You can do this by switching to the appropriate window, typing your Tmux prefix (commonly Ctrl-B or Ctrl-A) and then a colon to bring up a Tmux command line, and typing:

```
:setw synchronize-panes
```

You can optionally add on or off to specify which state you want; otherwise the option is simply toggled. This option is specific to one window, so it won’t change the way your other sessions or windows operate. When you’re done, toggle it off again by repeating the command. [tip source](http://blog.sanctum.geek.nz/sync-tmux-panes/)


## Misc

d  detach
t  big clock
?  list shortcuts
:  prompt
