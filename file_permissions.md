# Unix file permissions

|  P  | C | Description |
|-----|---|--------------------------|
| rwx | 7 | Read, write, and execute |
| rw- | 6 | Read and write           |
| r-x | 5 | Read and execute         |
| r-- | 4 | Read-only                |
| -wx | 3 | Write and execute        |
| -w- | 2 | Write-only               |
| --x | 1 | Execute-only             |
| --- | 0 | None                     |
