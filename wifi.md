# Wifi

1. List networks
```
nmcli dev wifi
```

2. Connect to network
```
sudo nmcli --ask dev wifi connect network-ssid
```

3. Put wifi adapter into monitor mode
```bash
sudo ip link set wlp1s0 down
sudo iw wlp1s0 set type monitor
sudo ip link set wlp1s0 up

sudo ip link set wlp1s0 down
sudo iw wlp1s0 set type managed
sudo ip link set wlp1s0 up
```

1. list devices
	nmcli dev status

2. is wifi device enabled?
	nmcli radio wifi

3. enable wifi
	nmcli radio wifi on

4. list wifi signals
	nmcli dev wifi list

5. connect wifi signal
	sudo nmcli dev wifi connect <<SSID>>
	sudo nmcli dev wifi connect <<SSID>> password "pwd"
	sudo nmcli --ask dev wifi connect <<SSID>>

6. list saved connections
	nmcli con show

7. disconnect connection
	nmcli con down ssid/uuid

8. connect connection
	nmcli con up ssid/uuid

9. nm-connection-editor

