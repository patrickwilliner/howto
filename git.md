# Git

## Cherry Picking

Cherry pick multiple revisions. Abort after merge conflict.

```bash
git cherry-pick refid refid
git cherry-pick --abort
```

Cherry pick, merge and continue.

```bash
git cherry-pick refid
# fix merge conflict
git add file.md
git cherry-pick --continue
```

## Git Stash

```bash
git stash
git stash pop
git stash list
git stash -m 'Navbar v1'
git stash pop --index 1
git stash drop stash@{0}
git stash clear
git stash show 1
```

## Git squash
```bash
git rebase -i HEAD~3 # interactive rebasing
```

## Git merge
```
git merge feature_1
git log --merge --oneline
git merge --abort
git status
```
