# Qemu

1. Install Guest Agent
```bash
apt update && apt -y install qemu-guest-agent
systemctl enable qemu-guest-agent
systemctl start qemu-guest-agent
```

1. Mount host share
```bash
mount -t 9p trans=virtio,version=9p2000.L,rw /dev/host /machine/host
```
