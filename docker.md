# Docker

## Run interactive shell in container
```bash
docker exec -it container_name /bin/bash
```

## Start busybox linux, connect it to network, run shell
```bash
docker run --network elastic_default -it --rm busybox
```
