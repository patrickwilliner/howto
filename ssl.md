# Self signed certificates

Generate private key for CA.

```bash
openssl genrsa -des3 -out willinerorg.key 2048
```

Generate root certificate.

```bash
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem
```

Generate browser certificate.

```bash
openssl genrsa -out hellfish.test.key 2048
openssl req -new -key hellfish.test.key -out hellfish.test.csr

Create file jupiter.ext

```
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = jupiter
```

Create certificate.

```bash
openssl x509 -req -in hellfish.test.csr -CA myCA.pem -CAkey myCA.key \
-CAcreateserial -out hellfish.test.crt -days 825 -sha256 -extfile hellfish.test.ext
```
