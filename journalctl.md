# Journalctl

## Show logs by priority
```bash
journalctl -p 2
```

## Priorities
0: emergency
1: alerts
2: critical
3: errors
4: warning
5: notice
6: info
7: debug

## Boot related commands

list boots
```bash
journalctl --list-boots
```

list certain book
```bash
journalctl -b -45
journalctl -b 8bab42c7e82440f886a3f041a7c95b98
journalctl -xb -45
```

Use -x -> add an explanation of the systemd error messages 
