# Netstat

## List open listening ports

```bash
netstat -tulpn
```

