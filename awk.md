# Awk

Here's an extensive cheat sheet for POSIX awk in markdown format:

## AWK Cheat Sheet

### Basic Structure

```awk
awk 'pattern { action }' input_file
```

### Command-Line Options

- `-F fs`: Set field separator
- `-v var=value`: Set variable
- `-f script_file`: Read program from file

### Built-in Variables

## Built-in Variables in AWK

### Input/Output Variables
- **FS**: Input field separator (default is whitespace)
- **OFS**: Output field separator (default is space)
- **RS**: Input record separator (default is newline)
- **ORS**: Output record separator (default is newline)

### Numbering Variables
- **NR**: Number of records processed so far
- **FNR**: Number of records processed in the current file
- **NF**: Number of fields in the current record

### Filename Variables  
- **FILENAME**: Name of the current input file
- **ARGV**: Array of command line arguments
- **ARGC**: Number of command line arguments

### Regex Variables
- **RSTART**: Starting position of the match found by the `match()` function
- **RLENGTH**: Length of the match found by the `match()` function

### Formatting Variables
- **OFMT**: Output format for numbers (default "%.6g")
- **CONVFMT**: Conversion format for numbers (default "%.6g")

### Miscellaneous
- **ENVIRON**: Array of environment variables
- **IGNORECASE**: Controls case sensitivity in string operations

### Patterns

- `/regex/`: Lines matching regex
- `expression`: Lines where expression is true
- `pattern1, pattern2`: Range of lines

### Control Structures

```awk
if (condition) {
    action
} else if (condition) {
    action
} else {
    action
}

while (condition) {
    action
}

for (init; condition; increment) {
    action
}

do {
    action
} while (condition)

for (var in array) {
    action
}
```

### Built-in Functions

#### Arithmetic Functions

- **sqrt(x)**: Square root of x
- **int(x)**: Integer part of x
- **rand()**: Random number between 0 and 1
- **srand([x])**: Set seed for rand(), returns previous seed
- **sin(x)**, **cos(x)**, **atan2(y,x)**: Trigonometric functions
- **exp(x)**, **log(x)**: Exponential and natural logarithm

#### String Functions

- **length([s])**: Length of string s (or $0 if omitted)
- **index(s,t)**: Position of string t in s, or 0 if not found
- **substr(s,m[,n])**: Extract n characters from s starting at position m
- **match(s,r)**: Position where regex r matches s, or 0 if no match
- **split(s,a[,r])**: Split s into array a using r as separator
- **sub(r,s[,t])**: Replace first match of r with s in string t (or $0)
- **gsub(r,s[,t])**: Replace all matches of r with s in string t (or $0)
- **sprintf(format,expr-list)**: Return formatted string

#### Input/Output Functions

- **getline**: Read next input record
- **print**: Print current record
- **printf(format,expr-list)**: Print formatted output
- **close(file)**: Close a file or pipe
- **system(command)**: Execute a system command

#### Time Functions

- **systime()**: Return current timestamp
- **strftime(format[,timestamp])**: Format timestamp as string

#### Bit Manipulation Functions (gawk-specific)

- **and(v1,v2)**, **or(v1,v2)**, **xor(v1,v2)**: Bitwise operations
- **compl(v)**: Bitwise complement
- **lshift(v,s)**, **rshift(v,s)**: Bit shift operations

#### Type Functions (gawk-specific)

- **isarray(x)**: Check if x is an array

### Arrays

```awk
array[index] = value
delete array[index]
```

### User-Defined Functions

```awk
function name(parameter-list) {
    statements
    return expression
}
```

### Special Patterns

- `BEGIN { actions }`: Execute before processing input
- `END { actions }`: Execute after processing input

### Examples

**Print specific fields:**

```awk
awk '{print $1, $3}' file
```

**Use custom field separator:**

```awk
awk -F: '{print $1}' /etc/passwd
```

**Sum values in a column:**

```awk
awk '{sum += $3} END {print sum}' file
```

**Count lines matching a pattern:**

```awk
awk '/pattern/ {count++} END {print count}' file
```

**Replace text:**

```awk
awk '{gsub(/old/, "new"); print}' file
```

**Calculate average:**

```awk
awk '{sum += $1; count++} END {print sum/count}' file
```

**Print lines longer than 80 characters:**

```awk
awk 'length($0) > 80' file
```

**Print every nth line:**

```awk
awk 'NR % 3 == 0' file
```

**Remove duplicate lines:**

```awk
awk '!seen[$0]++' file
```

**Print lines between two patterns:**

```awk
awk '/start/,/end/' file
```

This cheat sheet covers the core features of POSIX awk. Remember that GNU awk (gawk) and other implementations may offer additional features not included in the POSIX standard[1][2][3][4].

Citations:
[1] https://bl831.als.lbl.gov/~gmeigs/scripting_help/awk_cheat_sheet.pdf
[2] https://sweworld.net/cheatsheets/markdown/
[3] https://gist.github.com/Rafe/3102414
[4] https://quickref.me/awk.html
[5] https://gist.github.com/RyanSchu/72fe0751b3d528940611b18d9a50fc00
[6] https://www.gnu.org/software/gawk/manual/gawk.html
[7] https://stackoverflow.com/questions/20303826/how-to-highlight-bash-shell-commands-in-markdown/20304739
[8] https://cis106.com/docs/MarkdownCheatSheet/

---

```bash
awk -F '/' '/^\// {print $NF}' /etc/shells | uniq | sort
```
