# Bash

- Ctrl+L    Clear screen
- Ctrl+S    Stop output to screen
- Ctrl+Q    Resume output to screen
- Ctrl+A    Go to beginning of line
- Ctrl+E    Go to end of line
- Ctrl+B    Go left one character
- Alt+B     Go left one word
- Ctrl+F    Go right one character
- Alt+F     Go right one word
- Ctrl+D    Delete character under the cursor
- Alt+D     Delete all characters after the cursor on the current line
- Ctrl+H    Delete the character before the cursor
- Alt-T     Swap the current word with previous word
- Ctrl+T    Swap the last two characters before the cursor with each other
- Ctrl+_    Undo last key press
- Ctrl+W    Cut the word before the cursor
- Ctrl+K    Cut the part of the line after the cursor
- Ctrl+U    Cut the part of the line before the cursor
- Ctrl+Y    Paste
- Alt+U     Capitalize characters from the cursor to the end of the current word
- Alt+L     Uncapitalize characters from the cursor to the end of the current word
- Alt+C     Capitalize character under the cursor
- Ctrl+P    Get last command (resolves !!)

# Here documents

```bash
cat <<EOF > test.txt
111
222
333
EOF
```

# Bang syntax
```bash
cat file1 file2
!! # cat file1 file2
!$ # file2
!* # file1 file2
mv a-very-long-name{,.old}
```

# Misc
Alt+Shift+3 Prefix with # and enter
set -e # fail after first error
