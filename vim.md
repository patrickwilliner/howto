# Vim

S       Intend to the right position + INSERT MODE
dgg     Delete from cursor to top
dG      Delete from cursor to bottom
di"     Delete inside ""
yi"     Yank inside ""
gg=G    Indent entire document

## Navigation
H       Goto top (Hight) of screen
M       Goto center (Middle) of screen
L       Goto bottom (Low) of the screen

## Insert Mode
Ctrl+w  Delete word to the left
Ctrl+o  Run command

# Misc
:set spell                      Set spell check
:set spell spelllang=de_CH      Set spell check for lang de_CH
:!pwd                           Command mode
:read !ls                       read command into current buffer
:s /old/new/g                   global substitution on current line
:%s /old/new/g                  global substitution on every line
:5,11s /old/new/g               global substitution on lines 5 to 11
